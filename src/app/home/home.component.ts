import { Component, OnDestroy, OnInit } from '@angular/core';
import { ICharacter } from '../interfaces&classes/characters';
import { CFilterComic, iComic, IComicsResponse } from '../interfaces&classes/comics';
import { MarvelService } from '../services/marvel.service';
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
    aComics: Array<iComic>;
    isLoading: boolean;
    constructor(private oMarvelService: MarvelService) {
        this.aComics = new Array<iComic>();
        this.isLoading = false;
    }

    ngOnInit(): void {
        this.onSearch();
    }

    onSearch(oEvent?: ICharacter) {
        if (oEvent) {
            this.oMarvelService.oFIlterComic.characterid = oEvent.id;
        }
        this.isLoading = true;
        this.oMarvelService.getComics().subscribe((oRes: IComicsResponse) => {
            this.aComics = oRes.results;
            this.isLoading = false;
        });
    }

    ngOnDestroy() {
        this.oMarvelService.oFIlterComic = new CFilterComic();
    }
}
