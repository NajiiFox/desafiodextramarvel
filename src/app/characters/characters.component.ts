import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { ICharacter, ICharactersResponse } from '../interfaces&classes/characters';
import { MarvelService } from '../services/marvel.service';

@Component({
    selector: 'app-characters',
    templateUrl: './characters.component.html',
    styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
    oForm: FormGroup;
    aCharacters: Array<ICharacter>;
    nTotal: number;
    isLoading: boolean;
    constructor(private oMarvelService: MarvelService, oFormBuilder: FormBuilder) {
        this.aCharacters = new Array<ICharacter>();
        this.isLoading = false;
        this.oForm = oFormBuilder.group({
            edtCharacter: ['']
        });
    }

    ngOnInit(): void {
        this.onSearch();
    }

    onSearch(event?: PageEvent, nameStartsWith?: string) {
        this.isLoading = true;
        let limit = 10;
        let offset = 0;
        if (event) {
            limit = event.pageSize;
            offset = (event.pageIndex === 0 ? 0 : event.pageIndex + 1) * event.pageSize;
            if (offset >= this.nTotal) {
                offset = event.pageIndex * event.pageSize;
            }
        }

        this.oMarvelService
            .getCharacters(this.edtCharacter.value, nameStartsWith, offset, limit)
            .subscribe((oRes: ICharactersResponse) => {
                this.aCharacters = oRes.results;
                this.nTotal = oRes.total;
                this.isLoading = false;
            });
    }

    get edtCharacter() {
        return this.oForm.get('edtCharacter');
    }
}
