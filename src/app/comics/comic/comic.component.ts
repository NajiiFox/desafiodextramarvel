import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { iComic, IComicsResponse } from 'src/app/interfaces&classes/comics';
import { MarvelService } from 'src/app/services/marvel.service';

@Component({
    selector: 'app-comic',
    templateUrl: './comic.component.html',
    styleUrls: ['./comic.component.scss']
})
export class ComicComponent implements OnInit, AfterViewInit {
    nIdComic: number;
    oComic: iComic;
    isLoading: boolean;
    aImgs: Array<any>;
    constructor(private oRoute: ActivatedRoute, private oMarvelService: MarvelService) {
        this.aImgs = new Array<any>();
    }

    ngOnInit(): void {
        this.nIdComic = Number(this.oRoute.snapshot.paramMap.get('id'));
        this.onSearch(this.nIdComic);
    }

    ngAfterViewInit(): void {
        this.onCreateImgArray();
    }

    private onCreateImgArray() {
        this.aImgs.push({
            path: `${this.oComic.thumbnail.path}.${this.oComic.thumbnail.extension}`
        });
        this.oComic.images.forEach((oImg) => {
            const oImgLink = {
                path: `${oImg.path}.${oImg.extension}`
            };
            this.aImgs.push(oImgLink);
        });
    }

    private onSearch(nId: number) {
        this.isLoading = true;
        this.oMarvelService.getComicById(nId).subscribe((oRes: IComicsResponse) => {
            this.oComic = oRes.results[0];
            this.isLoading = false;
        });
    }
}
