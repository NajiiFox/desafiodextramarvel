import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { BehaviorSubject } from 'rxjs';
import { ICharacter, ICharactersResponse } from '../interfaces&classes/characters';
import { CFilterComic, iComic, IComicsResponse } from '../interfaces&classes/comics';
import { MarvelService } from '../services/marvel.service';

@Component({
    selector: 'app-comics',
    templateUrl: './comics.component.html',
    styleUrls: ['./comics.component.scss']
})
export class ComicsComponent implements OnInit, OnDestroy {
    aComics: Array<iComic>;
    nTotal: number;
    isLoading: boolean;
    oForm: FormGroup;

    constructor(private oMarvelService: MarvelService, oFormBuilder: FormBuilder) {
        this.aComics = new Array<iComic>();
        this.isLoading = false;
        this.oForm = oFormBuilder.group({
            edtTitle: [''],
            edtCharacter: ['']
        });
    }
    ngOnInit(): void {
        this.onSearch();
    }

    onFilter() {
        this.getCharaterByName(this.edtCharacter.value).subscribe(() => {
            this.oMarvelService.oFIlterComic.title = this.edtTitle.value;
            this.onSearch();
        });
    }

    onSearch(event?: PageEvent): void {
        this.isLoading = true;
        let limit = 10;
        let offset = 0;
        if (event) {
            limit = event.pageSize;
            offset = (event.pageIndex === 0 ? 0 : event.pageIndex + 1) * event.pageSize;
            if (offset >= this.nTotal) {
                offset = event.pageIndex * event.pageSize;
            }
        }

        this.oMarvelService.getComics(offset, limit).subscribe((oRes: IComicsResponse) => {
            this.aComics = oRes.results;
            this.nTotal = oRes.total;
            this.isLoading = false;
        });
    }

    ngOnDestroy() {
        this.oMarvelService.oFIlterComic = new CFilterComic();
    }

    private getCharaterByName(name: string) {
        const oSubject = new BehaviorSubject<ICharacter>(null);
        if (this.edtCharacter) {
            this.oMarvelService.getCharacters(name).subscribe((oRes: ICharactersResponse) => {
                this.oMarvelService.oFIlterComic.characterid = oRes.results[0].id;
                oSubject.next(oRes.results[0]);
            });
        }
        return oSubject;
    }

    get edtTitle() {
        return this.oForm.get('edtTitle');
    }
    get edtCharacter() {
        return this.oForm.get('edtCharacter');
    }
}
