import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from '../app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlphabetCarouselComponent } from './alphabet-carousel/alphabet-carousel.component';

@NgModule({
    declarations: [FooterComponent, AlphabetCarouselComponent],
    imports: [
        CommonModule,
        MaterialModule,
        AppRoutingModule,
        FlexLayoutModule,
        MDBBootstrapModule.forRoot(),
        TranslateModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
        FooterComponent,
        AlphabetCarouselComponent,
        AppRoutingModule,
        FlexLayoutModule,
        MDBBootstrapModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class SharedModule {}
