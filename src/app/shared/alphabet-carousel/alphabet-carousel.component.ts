import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import * as $ from 'jQuery';

@Component({
    selector: 'app-alphabet-carousel',
    templateUrl: './alphabet-carousel.component.html',
    styleUrls: ['./alphabet-carousel.component.scss']
})
export class AlphabetCarouselComponent implements OnInit {
    @Output() selected = new EventEmitter<string>();
    aAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    sLetterSelected: string;
    constructor() {}

    ngOnInit(): void {}

    onSelected(sLetterSelected: string) {
        this.sLetterSelected = sLetterSelected;
        this.selected.emit(sLetterSelected);
    }

    nextLeftScroll(): void {
        $('.out-section').animate(
            {
                scrollLeft: Math.max(0, $('.out-section').scrollLeft() - 148)
            },
            500
        );
    }

    nextRightScroll(): void {
        $('.out-section').animate(
            {
                scrollLeft: Math.max(2, $('.out-section').scrollLeft() + 148)
            },
            500
        );
    }
}
