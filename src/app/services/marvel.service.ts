import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { ICharactersResponse } from '../interfaces&classes/characters';
import { CFilterComic, IComicsResponse } from '../interfaces&classes/comics';

@Injectable({
    providedIn: 'root'
})
export class MarvelService {
    oFIlterComic: CFilterComic;

    constructor(
        private httpClient: HttpClient,
        protected matSnackBar: MatSnackBar,
        protected translateService: TranslateService
    ) {
        this.oFIlterComic = new CFilterComic();
    }

    getCharacters(name?: string, nameStartsWith?: string, offset?: number, limit?: number, orderBy?: string) {
        const oParam = {};
        oParam['ts'] = environment.nTs;
        oParam['apikey'] = environment.sKey;
        oParam['hash'] = environment.sHash;
        if (name) {
            oParam['name'] = name;
        }
        if (nameStartsWith) {
            oParam['nameStartsWith'] = nameStartsWith;
        }
        if (offset) {
            oParam['offset'] = offset;
        } else {
            oParam['offset'] = 0;
        }
        if (limit) {
            oParam['limit'] = limit;
        } else {
            oParam['limit'] = 10;
        }
        if (orderBy) {
            oParam['orderBy'] = orderBy;
        } else {
            oParam['orderBy'] = 'name';
        }
        return this.httpClient.get(`${environment.sUrl}v1/public/characters`, { params: oParam }).pipe(
            map((oRes: any) => {
                return {
                    total: oRes.data.total,
                    results: oRes.data.results
                } as ICharactersResponse;
            })
        );
    }

    getComics(offset?: number, limit?: number, orderBy?: string) {
        const oParam = {};
        oParam['ts'] = environment.nTs;
        oParam['apikey'] = environment.sKey;
        oParam['hash'] = environment.sHash;
        if (offset) {
            oParam['offset'] = offset;
        } else {
            oParam['offset'] = 0;
        }
        if (limit) {
            oParam['limit'] = limit;
        } else {
            oParam['limit'] = 10;
        }
        if (orderBy) {
            oParam['orderBy'] = orderBy;
        } else {
            oParam['orderBy'] = 'title';
        }
        if (this.oFIlterComic?.title) {
            oParam['title'] = this.oFIlterComic.title;
        }
        if (this.oFIlterComic?.characterid) {
            oParam['characters'] = this.oFIlterComic.characterid;
        }
        return this.httpClient.get(`${environment.sUrl}v1/public/comics`, { params: oParam }).pipe(
            map((oRes: any) => {
                return {
                    total: oRes.data.total,
                    results: oRes.data.results
                } as IComicsResponse;
            })
        );
    }

    getComicById(id: number) {
        const oParam = {};
        oParam['ts'] = environment.nTs;
        oParam['apikey'] = environment.sKey;
        oParam['hash'] = environment.sHash;

        return this.httpClient.get(`${environment.sUrl}v1/public/comics/${id}`, { params: oParam }).pipe(
            map((oRes: any) => {
                return {
                    total: oRes.data.total,
                    results: oRes.data.results
                } as IComicsResponse;
            })
        );
    }
}
