import { Component, Input, OnInit } from '@angular/core';
import { ICharacter } from 'src/app/interfaces&classes/characters';

@Component({
    selector: 'app-characters-grid',
    templateUrl: './characters-grid.component.html',
    styleUrls: ['./characters-grid.component.scss']
})
export class CharactersGridComponent implements OnInit {
    @Input() aCharacters: Array<ICharacter>;
    constructor() {}

    ngOnInit(): void {}
}
