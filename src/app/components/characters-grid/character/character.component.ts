import { Component, Input, OnInit } from '@angular/core';
import { ICharacter } from 'src/app/interfaces&classes/characters';

@Component({
    selector: 'app-character',
    templateUrl: './character.component.html',
    styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {
    @Input() oCharacter: ICharacter;
    constructor() {}

    ngOnInit(): void {}
}
