import { Component, Input } from '@angular/core';
import { iComic } from 'src/app/interfaces&classes/comics';

@Component({
    selector: 'app-comics-grid',
    templateUrl: './comics-grid.component.html',
    styleUrls: ['./comics-grid.component.scss']
})
export class ComicsGridComponent {
    @Input() aComics: Array<iComic>;

    constructor() {
        this.aComics = new Array<iComic>();
    }
}
