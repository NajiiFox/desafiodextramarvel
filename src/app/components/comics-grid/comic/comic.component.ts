import { Component, Input, OnInit } from '@angular/core';
import { iComic } from 'src/app/interfaces&classes/comics';

@Component({
    selector: 'app-comic',
    templateUrl: './comic.component.html',
    styleUrls: ['./comic.component.scss']
})
export class ComicComponent implements OnInit {
    @Input() oComic: iComic;
    constructor() {}

    ngOnInit(): void {}
}
