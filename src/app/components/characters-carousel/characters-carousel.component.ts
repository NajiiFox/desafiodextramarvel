import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import * as $ from 'jQuery';
import { ICharacter, ICharactersResponse } from 'src/app/interfaces&classes/characters';
import { MarvelService } from 'src/app/services/marvel.service';

@Component({
    selector: 'app-characters-carousel',
    templateUrl: './characters-carousel.component.html',
    styleUrls: ['./characters-carousel.component.scss']
})
export class CharactersCarouselComponent implements OnInit {
    aCharacters: Array<ICharacter>;
    nIdSelected: number;
    @Output() selected = new EventEmitter<ICharacter>();

    constructor(private oMarvelService: MarvelService) {
        this.aCharacters = new Array<ICharacter>();
    }

    ngOnInit(): void {
        this.onSearch();
    }

    onSelected(oCharacter: ICharacter) {
        this.nIdSelected = oCharacter.id;
        this.selected.emit(oCharacter);
    }

    nextLeftScroll(): void {
        $('.out-section-characters').animate(
            {
                scrollLeft: Math.max(0, $('.out-section-characters').scrollLeft() - 148)
            },
            500
        );
    }

    nextRightScroll(): void {
        $('.out-section-characters').animate(
            {
                scrollLeft: Math.max(2, $('.out-section-characters').scrollLeft() + 148)
            },
            500
        );
    }

    private onSearch() {
        this.oMarvelService.getCharacters('', '', 0, 50).subscribe((oRes: ICharactersResponse) => {
            this.aCharacters = oRes.results;
        });
    }
}
