import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../shared/material.module';
import { SharedModule } from '../shared/shared.module';
import { HomeCarouselComponent } from './home-carousel/home-carousel.component';
import { CharactersCarouselComponent } from './characters-carousel/characters-carousel.component';
import { CharactersGridComponent } from './characters-grid/characters-grid.component';
import { ComicsGridComponent } from './comics-grid/comics-grid.component';
import { ComicComponent } from './comics-grid/comic/comic.component';
import { CharacterComponent } from './characters-grid/character/character.component';

@NgModule({
    declarations: [
        HomeCarouselComponent,
        CharactersCarouselComponent,
        CharactersGridComponent,
        ComicsGridComponent,
        ComicComponent,
        CharacterComponent
    ],
    imports: [CommonModule, MaterialModule, SharedModule],
    exports: [
        HomeCarouselComponent,
        CharactersCarouselComponent,
        CharactersGridComponent,
        ComicsGridComponent,
        ComicComponent
    ]
})
export class ComponentsModule {}
