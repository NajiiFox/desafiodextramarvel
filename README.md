# Marvel

Queremos que você desenvolva um catálogo de quadrinhos da **Marvel Comics**, e, para isso será necessário que você desenvolva o front-end desta aplicação.
A **Marvel Comics** disponibiliza uma API onde estes dados podem ser encontrados, você deve se registrar na plataforma da Marvel para ter acesso a sua API, de forma gratuita, através do [link](https://developer.marvel.com)

# Requisitos necessários:

### Listagem dos quadrinhos ✅
* Título; ✅
* imagem; ✅
* thumbnail; ✅
* autores; ✅

### Paginação dos quadrinhos ✅

* Grid dos quadrinhos em 10 e 10 ✅

### Pesquisa por personagem ✅

* Pesquisa através do nome do personagem em inglês ✅

### Detalhe dos quadrinhos ✅

* Botão ou mecanismo similar para abertura de uma nova página com detalhes do quadrinho. ✅

### Qualidade 

* Criar testes para sua aplicação ❌ (Motivo: extrapolção do limite de tempo)

# Requisitos Opcionais 
### Se quiser, escolha um ou mais itens para implementar:

* Criar a aplicação responsiva; ✅
* Criar um filtro por letra inicial do nome do personagem que ao clicar, exiba todos os itens correspondentes; ✅
* Criar uma funcionalidade que permita que o usuário favorite os personagens; ❌ (Motivo: extrapolção do limite de tempo)
* Armazenar a data em que o item foi favoritado; ❌ (Motivo: extrapolção do limite de tempo)
* Depois de favoritar, é preciso ter uma página para visualizar os itens favoritos, ordenados por nome ou por data em que foi favoritado (com a seguinte rota: /comics/favorites); ❌ (Motivo: extrapolção do limite de tempo)
* Opção de remover um personagem de favoritos; ❌ (Motivo: extrapolção do limite de tempo)
* Opção para limpar todos os itens favoritados; ❌ (Motivo: extrapolção do limite de tempo)
* Hospedar sua aplicação em algum servidor cloud (se o fizer, envie o link da aplicação hospedada junto com o link do repositório); ❌ (Motivo: extrapolção do limite de tempo )


O código deve estar em um repositório público. ✅


# Bugs conhecidos:

* Página de comics, ao acessar a rota atravez de outra página a barra de pesquisa não funciona, tornando necessario recarregar a página. STATUS: ✅ Corrigido!

# MarvelDextra

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
